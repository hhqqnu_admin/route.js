//数据类型判断
var TYPE=(function(){
	var r={},types=['Arguments','Function','String','Number','Date','RegExp','Error','Null'];
	for(var i=0,t;t=types[i++];){
		!function(t){
			r['is'+t]=function(obj){
				return Object.prototype.toString.call(obj) === '[object '+t+']';
			}
		}(t)
	}
	return r;
})();